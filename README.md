# Deprecated

As of 2023-05-16, we discontinued the access request process with the release of [Apple Silicon (M1) GitLab runners on macOS - Beta](https://gitlab.com/groups/gitlab-org/-/epics/6105). From now on, all customers in GitLab Premium and Ultimate plans have access to these runners by simply using the `saas-macos-medium-m1`. tag in their ci/cd jobs.

```

.macos_saas_runners:
  tags:
    - saas-macos-medium-m1
  image: macos-12-xcode-14

stages:
  - build
  - test

before_script:
  - echo "started by ${GITLAB_USER_NAME}"

build:
  extends:
    - .macos_saas_runners
  stage: build
  script:
    - echo "running scripts in the build job"

test:
  extends:
    - .macos_saas_runners
  stage: test
  script:
    - echo "running scripts in the test job"



```


- **Note** - The GitLab SaaS Runners on macOS are not available to customers in the Free plan.

